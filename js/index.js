/**
 * Created by zzl81cn on 2017/2/3.
 */
var index = {
	/*primaryNav: function () {
		var toggleBtn = $('#navCollapse'),
			slideNav = $('#primaryNav');
		toggleBtn.click(function () {
			slideNav.animate({width: 'toggle',opacity: 'toggle'}, 200);
		});
	},*/
	getMenu: function () {
		$.ajax({
			url:"js/menu.json",
			type: 'get',
			success: function(data){
				console.log('data is: ', data, data.length);
				$('#categoryList').html(function () {
					var categoryMenu = '';
					for(var i = 0; i < data.length; i++) {
						categoryMenu += '<li><a href="javascript:void(0);"><span class="fa '+ data[i].rootList.icon+'"></span><h3 class="text">'+data[i].rootList.name+'</h3></a></li>';
					}
					return categoryMenu;
				});
				$('#navList').empty().html(function () {
					var submenuList = '';
					for(var i = 0; i< data.length; i++) {
						submenuList += '<li class="nav-li"><ul class="nav-sublist">';
						for(var j = 0; j < data[i].subList.length; j++) {
							submenuList += '<li><a data-target="'+data[i].subList[j].listTarget+'" href="">'+data[i].subList[j].listName+'</a></li>';
							// submenuList += '<li>'+data[i].subList[j].listName + data[i].subList[j].listTarget  +'</li>';
							// console.log('sublist',data[i], data[i].subList[j]);
						}
						submenuList += '</ul></li>';
					}
					return submenuList;
				});
				index.primaryNavClick();
			}
		})
	},
	primaryNavClick: function () {
		var $primaryNav = $('#primaryNav'),
			$categoryList = $('#categoryList'),
			$categoryListEl = $('#categoryList').find('li'),
			$navList = $('#navList').find('.nav-li'),
			$navSubListEl = $('.nav-sublist').find('li a');
		$categoryListEl.first().addClass('active');
		$navList.first().show();
		$navSubListEl.first().parent('li').addClass('active');

		var $toggleBtn = $('#toggleBtn'),
			$slideNav = $('#primaryNav');

		$toggleBtn.click(function () {
			$primaryNav.animate({width: 'toggle',opacity: 'toggle'}, 200);
			$(this).toggleClass('active');
		})

		/*$categoryList.parent().show().delay(2000).animate({width: 'toggle',opacity: 'toggle'}, 200);

		$primaryNav.hover(
			function () {
				$(this).find('#categoryList').animate({width: 'toggle',opacity: 'toggle'}, 200);
			},
			function () {
				$(this).find('#categoryList').animate({width: 'toggle',opacity: 'toggle'}, 200);
			}
		);*/

		// 二级菜单
		$categoryListEl.on('click', function (e) {
			$(this).addClass('active').siblings().removeClass('active');
			var $index = $categoryListEl.index(this);
			$navList.eq($index).show().siblings().hide();
			$('#breadLink').html($(this).find('.text').text());
		});
		$navSubListEl.click(function (e) {
			e.preventDefault();
			$(this).parent('li').addClass('active').siblings().removeClass('active');
			$('#mainContent').attr('src', $(this).attr('data-target'));
			console.log('Currents attr targets is: ', $(this).attr('data-target'));
			// return false;
		});

		// 计算视口适配尺寸
		var navHeight = parseInt($('#nav').css('height'));
		var tmpHeight = window.innerHeight,
			tmpWidth = window.innerWidth,
			heightResult = tmpHeight - (navHeight + 21),
			widthResult = tmpWidth - 150;

		$('#mainContent').css({'height': heightResult +'px','width': tmpWidth + 'px'});
	}
};
// index.primaryNav();
// index.primaryNavClick();
index.getMenu();