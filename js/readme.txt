// rootList为一级菜单，subList为二级菜单；
// rootList的"name"为菜单名称，并对应每一次切换菜单所对应的二级菜单最左侧名称，"icon"对应图标字体（http://fontawesome.io/icons/）；
// 每一个rootList包含一个subList

JSON数据
[
  {"rootList": {"name": "旅游营销 - 市场分析", "icon": "fa-bar-chart"},
    "subList": [
      {"listName":"首页", "listTarget": "https://www.baidu.com"},
      {"listName":"百度", "listTarget": "https://www.baidu.com"},
      {"listName":"有道", "listTarget": "https://www.youdao.com"},
      {"listName":"百度", "listTarget": "https://www.baidu.com"}
    ]
  },
  {"rootList": {"name": "旅游营销", "icon": "fa-pie-chart"},
    "subList": [
      {"listName":"百度2", "listTarget": "https://www.baidu.com"},
      {"listName":"有道2", "listTarget": "https://www.youdao.com"},
      {"listName":"百度2", "listTarget": "https://www.baidu.com"}
    ]
  },
  {"rootList": {"name": "市场分析", "icon": "fa-desktop"},
    "subList": [
      {"listName":"百度3", "listTarget": "https://www.baidu.com"},
      {"listName":"有道3", "listTarget": "https://www.youdao.com"}
    ]
  }
]